<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Libros</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="layaut/link_fonts.php">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" 
    integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
</head>
<body>
    <?php include_once "layaut/header.php"; ?>
        <main >
        <div class="carrusel_libros">
                <div class="slider_carrusel" >
                <section class="contenido_slider">
                        <div class="cajon_libro">
                            <div>
                                <div class="formato_libro1">
                                <h2 class="titulo_libro">Los ojos del Perro Siberiano</h2>
                                </div>
                                <h3>Autor: </h3>
                                <h3>Antonio Santa Ana</h3>
                                <p>Un libro que nos invita a recapacitar sobre el impacto de esta enfermedad en la sociedad y como los afectados
                                    no sólo sufren por razones de salud, sino que además tienen que lidiar con el prejucio social y muchas veces por 
                                    la discriminaciòn de sus propios familiares.
                                    El libro nos enseña como la mirada de todos cambian al enterarse de esta enferemedad, todos menos los ojos del perro siberano, 
                                    ojos que siempre miraràn igual, no importa si estas sasno o enfermo. </p>
                                <a href="https://books.google.com.pe/books/about/Los_ojos_del_perro_siberiano.html?id=r_n3OQf9nCYC&printsec=frontcover&source=kp_read_button&redir_esc=y#v=onepage&q&f=false"
                                    target="_blank">Ir al link</a>
                            </div>
                        </div>
                        <img src="images/libro12.png" alt="">
                    </section>
                    <section class="contenido_slider">
                    <div class="cajon_libro">
                            <div>
                                <div class="formato_libro2">
                                <h2 class="titulo_libro">Los ojos de mi princesa</h2>
                                </div>
                                <h3>Autor: </h3>
                                <h3>Carlos Cuauhtémoc Sánchez</h3>
                                <p>Libro para adolescentes que trata los problemas de la juventud,la idealización de la persona amada, la ilusión del amor, etc.
                                El personaje principal Jose Carlos idealiza a la mujer que amaba creando un ser prefecto al que el llamaba "princesa" y por la cual estrá dispuesto hacer todo, 
                                pronto Jose Carlos descubrirá la realidad, una realidad dolorosa, que le enseñara que no todo es color de rosa... Te animas a leerlo. </p>
                                <a href="https://es.bookmate.com/books/X7OmzWiY" target="_blank">Ir al link</a>
                            </div>
                        </div>
                        <img src="images/libro2.png" alt="">
                    </section>
                    <section class="contenido_slider">
                    <div class="cajon_libro">
                            <div>
                                <div class="formato_libro3">
                                <h2 class="titulo_libro">Un monstruo viene a verme</h2>
                                </div>
                                <h3>Autor: </h3>
                                <h3>Patrick Ness</h3>
                                <p>Aunque algunos la tachan de lenta y excesivamente efectista, «Un monstruo viene a verme» demuestra una gran profundidad psicológica al 
                                    tratar algunos temas con los que todos nos enfrentamos en la vida: la culpa, las relaciones humanas, la aceptación, el proceso de duelo y
                                    la gestión de las emociones. Connor un niño de 13 años con una madre diagnosicada con una enfermedad terminal, Connor siente culpa...
                                     Quieres saber por qué??? Leelo</p>
                                <a href="https://books.google.com/books?id=vPoOAwAAQBAJ&printsec=frontcover#v=onepage&q&f=false" target="_blank">Ir al link</a>
                            </div>
                        </div>
                        <img src="images/libro3.png" alt="">
                    </section>
                    <section class="contenido_slider">
                    <div class="cajon_libro">
                            <div>
                                <div class="formato_libro1 ">
                                <h2 class="titulo_libro">Los ojos del Perro Siberiano</h2>
                                </div>
                                <h3>Autor: </h3>
                                <h3>Antonio Santa Ana</h3>
                                <p>Un libro que nos invita a recapacitar sobre el impacto de esta enfermedad en la sociedad y como los afectados
                                    no sólo sufren por razones de salud, sino que además tienen que lidiar con el prejucio social y muchas veces por 
                                    la discriminaciòn de sus propios familiares.
                                    El libro nos enseña como la mirada de todos cambian al enterarse de esta enferemedad, todos menos los ojos del perro siberano, 
                                    ojos que siempre miraràn igual, no importa si estas sasno o enfermo. </p>
                                <a href="https://books.google.com.pe/books/about/Los_ojos_del_perro_siberiano.html?id=r_n3OQf9nCYC&printsec=frontcover&source=kp_read_button&redir_esc=y#v=onepage&q&f=false">Ir al link</a>
                            </div>
                        </div>
                        <img src="images/libro12.png" alt="">
                    </section>
                </div>
        </div>
        </main>
    <?php include_once "layaut/footer.php"; ?>
    <script src="mainL.js"></script>
</body>
</html>