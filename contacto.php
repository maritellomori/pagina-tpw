<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contacto</title>
    <!-- <script src="main.js"></script> -->
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" 
    integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
</head>
<body>
    <?php include_once "layaut/header.php"; ?>
    <main>
        <!-- <div class="cuerpof"> -->
            <div class="marca_name">
                <div id="marca_lastname"></div>
                <div id="marca_surname"></div>
            </div>
            <div class="negro" >
                <div id="rosa">                   
                </div> 
                <div id="contenedor">
                    <div class="texto_contacto">
                        <h2>Contactame...</h2>
                        <form action="process.php" method="post">
                            <div class="formulario">
                                <label for="name">Nombre:</label>
                            <input name="name" class="name" type="text" placeholder="Escribe tu nombre..." required>
                            </div>
                            <div class="formulario">
                                <label class="surname" for="surname">Apellido:</label>
                                <input name="surname" class="surname" type="text" placeholder="Escribe tu apellido... " required>
                            </div>
                            <div class="formulario">
                                <label for="email">Correo:</label>
                                <input name="email" class="email" type="email" placeholder="Escribe tu correo..." required>
                            </div>
                            <div class="formulario">
                                <label for="phone">Celular:</label>
                                <input name="phone" class="phone" type="text" placeholder="Escribe tu teléfono..." required>
                            </div>
                            <div class="formulario">
                                <label>¿Qué contenido te gustaría que se publique en la página?</label> 
                            </div>
                            <div class="opinion">
                                <textarea name="opinion" class="opinion" width="50px"></textarea>
                            </div>
                            <div class="submit" >
                                <input type="submit">
                            </div>
                        </form>
                    </div>

                </div>
            </div> 
        <!-- </div> -->
    </main>
    <?php include_once "layaut/footer.php"; ?>
</body>
</html>