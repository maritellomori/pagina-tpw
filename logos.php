<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Logos</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" 
    integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
</head>
<body>
    <?php include_once "layaut/header.php"; ?>
    <main>
        <div class="marca_name">
            <div id="marca_lastname"></div>
            <div id="marca_surname"></div>
        </div>
        <div class="cuerpof">
            <div class="texto-seccion">
                <div id="seccion">
                    <h1>Logos</h1>
                </div>
        
                <div id="resumen">
                    <h3>Resumen de la sección</h3>
                    <ul class="listaf">
                        <li><a href="subseccion1.html">Enlace Subsección 1</a></li>
                        <li><a href="subseccion2.html">Enlace Subsección 2</a></li>
                        <li><a href="subseccion3.html">Enlace Subsección 3</a></li>
                        <li><a href="subseccionn.html">Etc</a></li>
                    </ul>
                </div>
            </div>
            <div class="imagen-seccion">
                <div id="imagen">
                    <h1>Foto de sección</h1>
                    <!-- <img src="" alt="foto de seccion"> -->
                </div>
            </div>    
        </div>
    </main>
    <?php include_once "layaut/footer.php"; ?>
</body>
</html>