<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" 
    integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
</head>
<body>
    <?php include_once "layaut/header.php"; ?>
    <main>
        <div class="cuerpo">
            <div class="caja1" >
                <div class="espacio1">
                    <a href="libros.php"> 
                    <h3>Libros</h3>
                    </a>
                </div>
                <div class="espacio2">
                    <a href="#"> 
                    <h3>Documentales</h3>
                    </a>
                </div>
            </div>
            <div class="caja2">
                <div class="espacio3">
                    <a href="#"> 
                    <h3>Películas</h3>
                    </a>
                </div>
                <div class="espacio4">
                    <a href="#"> 
                    <h3>Cursos Gratuitos de programación</h3>
                    </a>
                </div>         
            </div>
        </div>
    </main>
    <?php include_once "layaut/footer.php";?>
</body>
</html>