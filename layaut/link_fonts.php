    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" 
    integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link href = "https://fonts.googleapis.com/css2?family=Paprika&display=swap" rel = "stylesheet">
    <link href = "https://fonts.googleapis.com/css2?family=Quintessential&display=swap" rel = "stylesheet">
    <link href = "https://fonts.googleapis.com/css2?family=Dancing+Script&display=swap" rel = "stylesheet">
    <link href = "https://fonts.googleapis.com/css2?family=Kaushan+Script&display=swap" rel = "stylesheet">
    <link href = "https://fonts.googleapis.com/css2?family=Emilys+Candy&display=swap" rel = "stylesheet">
