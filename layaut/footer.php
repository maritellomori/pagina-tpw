<footer>
        <div class="container-footer">
            <div class="footera">
                <div class="imagen-footera">
                </div>
                <div class="cuerpo-footera">
                    <div class="cuerpo-titulo">
                        <h3>Acerca</h3>
                    </div>
                    <div class="cuerpo-conteidoa">
                        <ul class="listacerca">
                            <li><a href="contacto.html">Quién soy?</a></li>
                            <li><a href="https://www.youtube.com/channel/UC0ntvHJfviqvUHzdGBgrKlA">Me gusta Videos</a></li>
                            <li><a href="https://www.instagram.com/DanielaTello/">Me gusta Fotos</a></li>
                        </ul>
                    </div>
                    <div class="cuerpo-botona">
                        
                    </div>
                </div>
            </div>
            <div class="footerp">
                <div class="imagen-footerp">

                </div>
                <div class="cuerpo-footerp">
                    <div class="cuerpo-titulo">
                        <h3>Portafolio</h3>
                    </div>
                    <div class="cuerpo-conteidop">
                        <ul class="listporta" >
                            <li><a href="logos.php">Logos</a></li>
                            <li><a href="promocionales.php">Promocionales</a></li>
                            <li><a href="fotografia.php">Fotografia</a></li>
                        </ul>
                    </div>
                    <div class="cuerpo-botonp">
                        
                    </div>
                </div>
            </div>
        </div>
                <div class="pagina">
                    <img class="img5" src="images/WhatsApp Image 2020-09-06 at 17.40.13.jpeg" width="150" alt="Mi fotografía" title="Mi Fotografía">
                    <div>
                        <h3 class="h3p">Pagina web diseñada por Daniela Tello</h3>
                    </br>
                        <a href="contacto.php">Contacto</a>
                    </div>
                </div>
            </div>
        <div class="footers">
                <div class="contactos">
                    <div class="f">
                        <a href="https://www.facebook.com/"><i class="fab fa-facebook"></i></a>
                    </div>
                    <div class="i">
                        <a href="https://www.instagram.com/"><i class="fab fa-instagram-square"></i></a>
                        
                    </div>
                    <div class="t">
                        <a href="https://twitter.com/"><i class="fab fa-twitter"></i></a>
                    </div>
                    <div class="y">
                        <a href="https://www.youtube.com/">
                        <i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>